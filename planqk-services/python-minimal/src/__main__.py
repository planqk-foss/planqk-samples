import json
import os

from planqk.commons.constants import OUTPUT_DIRECTORY_ENV
from planqk.commons.json import any_to_json
from planqk.commons.logging import init_logging
from planqk.commons.runtime.output import write_string_output

from .program import run, InputData

init_logging()

# This file is executed you run `python -m src` from the project root. Use this file to test your program locally.
# You can read the input data from the `input` directory and map it to the respective parameter of the `run()` function.

# Redirect PLANQK's output directory for local testing
directory = "./out"
os.makedirs(directory, exist_ok=True)
os.environ[OUTPUT_DIRECTORY_ENV] = directory

with open(f"./input/data.json") as file:
    data = InputData.model_validate(json.load(file))

result = run(data)

write_string_output("output.json", any_to_json(result))
