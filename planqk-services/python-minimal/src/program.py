from typing import List

from pydantic import BaseModel


class InputData(BaseModel):
    values: List[float]


class CalculationResult(BaseModel):
    sum: float


def run(data: InputData) -> CalculationResult:
    return CalculationResult(sum=sum(data.values))
