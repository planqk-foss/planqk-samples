# python-minimal

## Run with Docker

Build the Docker image:

```bash
docker build --pull -t python-minimal .
```

Start the Docker container:

```bash
PROJECT_ROOT=(`pwd`)
docker run -it \
  -v $PROJECT_ROOT/input:/var/runtime/input \
  -v $PROJECT_ROOT/out:/var/runtime/output \
  python-minimal
```

> **HINT**:
> For GitBash users on Windows, replace
> ```bash
> PROJECT_ROOT=(`pwd`)
> ```
> with
> ```bash
> PROJECT_ROOT=(/`pwd`)
> ```
> For Windows command-prompt users, you can define the volume mounts using `-v %cd%/input:/var/runtime/input` and `-v %cd%/out:/var/runtime/output`.
