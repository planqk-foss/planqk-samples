# node-service

Example service that calculates the sum of an array of numbers.

## Run with Node

```shell
node .
```

## Run with Docker

Build the Docker container:

```shell
docker build -t node-service .
```

Run the Docker container:

```shell
PROJECT_ROOT=(`pwd`)
rm -rf $PROJECT_ROOT/out
docker run -it \
  -v $PROJECT_ROOT/input:/var/runtime/input \
  -v $PROJECT_ROOT/out:/var/runtime/output \
  node-service
```

> **HINT**:
> For GitBash users on Windows, replace
> ```bash
> PROJECT_ROOT=(`pwd`)
> ```
> with
> ```bash
> PROJECT_ROOT=(/`pwd`)
> ```
>
> For Windows command-prompt users, you can define the volume mounts using `-v %cd%/input:/var/runtime/input` and `-v %cd%/out:/var/runtime/output`.
