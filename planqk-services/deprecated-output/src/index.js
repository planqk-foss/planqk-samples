const fs = require('fs');

function listInputFiles() {
  const inputDirectory = fs.existsSync('/var/runtime/input') ? '/var/runtime/input' : './input';

  const inputFiles = {}

  fs.readdirSync(inputDirectory).forEach((file) => {
    const [name, _] = file.split('.');
    inputFiles[name] = `${inputDirectory}/${file}`;
  });

  return inputFiles;
}

function readJsonFile(filepath, defaultValue = null) {
  try {
    fs.accessSync(filepath);
    return JSON.parse(fs.readFileSync(filepath));
  } catch {
    return defaultValue;
  }
}

async function run() {
  console.time("run");

  console.info("Reading input files...");
  const inputFiles = listInputFiles();

  if (!inputFiles.data) {
    console.error("Property 'data' not found");
    process.exit(1);
  }

  const inputData = readJsonFile(inputFiles.data);
  console.debug('Input data:', inputData);

  console.info('Calculating sum...');
  console.info('Sum:', 0)

  // Sum the values in the "values" property
  const sum = await inputData.values.reduce(async (acc, val) => {
    const s = await acc + val;
    console.info('Sum:', s)
    return s;
  }, 0);

  console.info('Calculation finished:', sum);

  console.log("PlanQK:Job:MultilineResult")
  console.log(JSON.stringify({ sum }, null, 2));
  console.log("PlanQK:Job:MultilineResult")

  console.log('Done!')
  console.timeEnd("run");
}

run();
