const fs = require('fs');

function delay(time) {
  return new Promise(resolve => setTimeout(resolve, time));
}

function listInputFiles() {
  const inputDirectory = fs.existsSync('/var/runtime/input') ? '/var/runtime/input' : './input';

  const inputFiles = {}

  fs.readdirSync(inputDirectory).forEach((file) => {
    const [name, _] = file.split('.');
    inputFiles[name] = `${inputDirectory}/${file}`;
  });

  return inputFiles;
}

function readJsonFile(filepath, defaultValue = null) {
  try {
    fs.accessSync(filepath);
    return JSON.parse(fs.readFileSync(filepath));
  } catch {
    return defaultValue;
  }
}

function writeJsonOutput(filename, output) {
  const outputDirectory = fs.existsSync('/var/runtime/output') ? '/var/runtime/output' : './out';

  if (!fs.existsSync(outputDirectory)) {
    fs.mkdirSync(outputDirectory);
  }

  fs.writeFileSync(`${outputDirectory}/${filename}`, JSON.stringify(output, null, 2));
}

async function run() {
  console.time("run");

  console.info("Reading input files...");
  const inputFiles = listInputFiles();

  if (!inputFiles.data) {
    console.error("Property 'data' not found");
    process.exit(1);
  }

  const inputData = readJsonFile(inputFiles.data);
  console.debug('Input data:', inputData);

  await delay(1000)

  console.info('Calculating sum...');
  console.info('Sum:', 0)

  // Sum the values in the "values" property
  const sum = await inputData.values.reduce(async (acc, val) => {
    const s = await acc + val;
    await delay(10000)
    console.info('Sum:', s)
    return s;
  }, 0);

  console.info('Calculation finished:', sum);

  await delay(10000)

  writeJsonOutput("output.json", { sum })

  console.log('Done!')
  console.timeEnd("run");
}

run();
