{
 "cells": [
  {
   "cell_type": "markdown",
   "id": "4eb4dd07-3f32-478b-93c0-409163de2e2d",
   "metadata": {},
   "source": [
    "# Local Development and Testing\n",
    "\n",
    "This tutorial demonstrates how to develop and test the service locally, enabling you to create service executions, monitor their status, retrieve their results, and cancel their executions.\n",
    "\n",
    "> **Prerequisites:**\n",
    "> Ensure that Docker is installed and running properly.\n",
    "\n",
    "Follow our [quickstart guide](https://docs.platform.planqk.de/quickstart.html) and [install the PLANQK CLI](https://docs.platform.planqk.de/quickstart.html#installation).\n",
    "\n",
    "From the root directory of your project, run the following command to \"serve\" and expose your project through a local web server:\n",
    "\n",
    "```bash\n",
    "planqk serve\n",
    "```\n",
    "\n",
    "The `planqk serve` command runs your current service code in a containerized environment to expose it through a local web server, similarly to how the PLANQK Platform would run your code.\n",
    "The local web server exposes the same RESTful HTTP endpoints to start a service execution, to check the status of running executions, to cancel executions, and to retrieve execution results.\n",
    "\n",
    "Once the server is up and running, you can utilize <http://localhost:8081> as the URL to manage service executions.\n",
    "For additional details regarding `planqk serve`, please refer to the documentation available [here](https://docs.platform.planqk.de/cli-reference.html#planqk-serve).\n",
    "\n",
    "From here, you either use the interactive API client available at <http://localhost:8081/docs> or you can use the **PLANQK Service SDK** to programmatically execute your service.\n",
    "\n",
    "Below we will show how you may utilize the **PLANQK Service SDK**."
   ]
  },
  {
   "cell_type": "markdown",
   "id": "68fa9461",
   "metadata": {},
   "source": [
    "Make sure you installed the latest version of the PLANQK Service SDK."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": null,
   "id": "364a78f7",
   "metadata": {},
   "outputs": [],
   "source": [
    "import sys\n",
    "\n",
    "!{sys.executable} -m pip install --upgrade planqk-service-sdk"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "c8357df8-2375-4e7a-b005-2648a3a3c06d",
   "metadata": {},
   "source": [
    "Create an instance of the `PlanqkServiceClient`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 25,
   "id": "e94dd115-e792-4974-8edb-161331249143",
   "metadata": {},
   "outputs": [],
   "source": [
    "from planqk.service.client import PlanqkServiceClient\n",
    "\n",
    "service_endpoint = \"http://localhost:8081\"\n",
    "\n",
    "# ... or if you are using Development Containers (devcontainers):\n",
    "# service_endpoint = \"http://host.docker.internal:8081\"\n",
    "\n",
    "client = PlanqkServiceClient(service_endpoint, None, None)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "e9c3c40a-44dd-4727-bdc1-f68594734ca0",
   "metadata": {},
   "source": [
    "Prepare the input data and parameters.\n",
    "In this case, we load the content of the respective JSON files from the `input` directory of the project."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 23,
   "id": "24afa15c-52eb-4c2a-8a1a-9378a75cbaed",
   "metadata": {},
   "outputs": [],
   "source": [
    "import json\n",
    "\n",
    "with open(\"../input/data.json\", \"r\") as file:\n",
    "    data = json.load(file)\n",
    "\n",
    "with open(\"../input/params.json\", \"r\") as file:\n",
    "    params = json.load(file)\n"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ecd6cf10",
   "metadata": {},
   "source": [
    "Start a service execution by passing the prepared input arguments."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 32,
   "id": "a3f503f5",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "id: 2fbf9b29-1e44-4018-8485-086c39722abe\n",
      "status: JobStatus.PENDING\n"
     ]
    }
   ],
   "source": [
    "job = client.start_execution(data=data, params=params)\n",
    "\n",
    "print(\"id:\", job.id)\n",
    "print(\"status:\", job.status)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "081dbdb8-6cb7-4cc8-9993-fcca36f2701b",
   "metadata": {},
   "source": [
    "You may check the status of your job by regularly calling `get_status()`."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 34,
   "id": "956ea044-2c83-4d8d-9a6c-fa2fc149f74b",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "status: JobStatus.SUCCEEDED\n"
     ]
    }
   ],
   "source": [
    "job = client.get_status(job.id)\n",
    "\n",
    "print(\"status:\", job.status)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "ae5752c4-df03-422d-9e8f-050637e353dc",
   "metadata": {},
   "source": [
    "Once the job has been succeeded, use `get_result()` to retrieve the job result."
   ]
  },
  {
   "cell_type": "code",
   "execution_count": 36,
   "id": "c6ca5cb1-2d95-4d01-b010-42b55699e325",
   "metadata": {},
   "outputs": [
    {
     "name": "stdout",
     "output_type": "stream",
     "text": [
      "{'metadata': None, 'result': {'sum': 42}}\n"
     ]
    }
   ],
   "source": [
    "from planqk.service.client import JobStatus\n",
    "\n",
    "if job.status is not JobStatus.SUCCEEDED:\n",
    "    raise Exception(\"Job did not succeed\")\n",
    "\n",
    "result = client.get_result(job.id)\n",
    "\n",
    "print(result)"
   ]
  },
  {
   "cell_type": "markdown",
   "id": "6570a0ba",
   "metadata": {},
   "source": [
    "From here, you may process the job result as needed, e.g., by saving it to a file or visualizing it."
   ]
  }
 ],
 "metadata": {
  "kernelspec": {
   "display_name": "Python 3 (ipykernel)",
   "language": "python",
   "name": "python3"
  },
  "language_info": {
   "codemirror_mode": {
    "name": "ipython",
    "version": 3
   },
   "file_extension": ".py",
   "mimetype": "text/x-python",
   "name": "python",
   "nbconvert_exporter": "python",
   "pygments_lexer": "ipython3",
   "version": "3.12.3"
  }
 },
 "nbformat": 4,
 "nbformat_minor": 5
}
