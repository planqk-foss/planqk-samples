import time
from typing import Dict, Any

from planqk.qiskit import PlanqkQuantumProvider
from pydantic import BaseModel, Field
from qiskit import QuantumCircuit, transpile


class InputData(BaseModel):
    n_coin_tosses: int = Field(default=4)


class CalculationResult(BaseModel):
    counts: Dict[str, Any]
    elapsed_time: float


def run(data: InputData) -> CalculationResult:
    """
    Coin Toss: Here we build a circuit (quantum algorithm) that performs n coin tosses on a
    Quantum Computer. Instead of heads and tails, we work with 0s and 1s: there are 2^n possible
    outcomes, and each time we measure the quantum state, we observe one of these outcomes. For
    example, with n=4 tosses, we have 16 possible outcomes, while n=10 tosses would give us 1024
    possible outcomes.
    """
    circuit = QuantumCircuit(data.n_coin_tosses)
    for i in range(data.n_coin_tosses):
        circuit.h(i)
    circuit.measure_all()

    # Use the PLANQK CLI and log in with "planqk login" or set the environment variable PLANQK_PERSONAL_ACCESS_TOKEN.
    # Alternatively, you can pass the access token as an argument to the constructor
    # provider = PlanqkQuantumProvider(access_token=access_token)
    provider = PlanqkQuantumProvider()

    backend_name = "azure.ionq.simulator"
    backend = provider.get_backend(backend_name)

    start_time = time.time()

    circuit = transpile(circuit, backend)
    job = backend.run(circuit, shots=100)
    counts = job.result().get_counts()

    elapsed_time = time.time() - start_time
    print(counts)

    # Cast values to integers
    counts = {str(k): int(v) for k, v in counts.items()}

    return CalculationResult(counts=counts, elapsed_time=elapsed_time)
