import json

from planqk.commons.json import any_to_json
from planqk.commons.logging import init_logging

from .program import run, InputData

init_logging()

with open(f"./input/data.json") as file:
    data = InputData.model_validate(json.load(file))

result = run(data)

print()
print(any_to_json(result))
