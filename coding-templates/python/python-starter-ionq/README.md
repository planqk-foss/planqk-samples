# PLANQK Starter Template

This is the PLANQK starter template for Python projects.
With only a few more steps, your quantum code is ready to be deployed as a PLANQK Service!

## Project Structure

Your code must be structured in a (not too) specific way.
The template provides a basic structure for your project:

```
.
├── .dockerignore
├── .gitignore
├── .planqkignore
├── .python-version
├── Dockerfile
├── pyproject.toml
├── README.md
├── requirements.txt
├── input
│   └── ...
└── src
    ├── __init__.py
    ├── __main__.py
    └── program.py
``` 

## Run the Starter Template

### Setup the environment

We recommend creating a dedicated Python environment to install and track all required packages from the start.
You may use the `requirements.txt` file to create a virtual environment with the tooling of your choice.
In the following, we use [`uv`](https://github.com/astral-sh/uv) to create a new virtual environment and install the required packages:

```bash
uv venv
uv sync

source .venv/bin/activate
```

### Run the code

You can run the code by executing the following command:

```bash
python -m src
```

By this, the `src` folder is executed as a Python module and the `__main__.py` is executed.
The `__main__.py` is prepared to read the input data from the `input` directory and to call the `run()` method of `program.py`.

This is helpful for local testing.
Locally, you can test your code with a JSON-conform input that gets imported within the `__main__`-method.
You can use the files in the `input` folder to provide input data and parameters for local testing.
You may adjust the `__main__`-method to, for example, load a different sets of input data from the `input` folder or to execute the `run()` method with some static input.

However, when bundling the code and deploying it to PLANQK, the runtime will call the `run()` method directly with the input data provided via the API.

## Extending the starter template

From the start, you should be able to run `python -m src` from within the project folder.

To extend the project, the most important method is the `run()` method inside `program.py`.
You may adapt and extend the code inside this method to your needs.
If you have written packages by yourself, which are required for your service, you can simply put them into separate Python packages within the `src` folder.
Any required Python package (like `numpy`, `pandas`, ...) must be mentioned within, you guessed it, the `requirements.txt` file.
Once you have installed your dependencies to local virtual environment, you can import these packages within any Python file needed.

> **IMPORTANT:**
> Do not rename either the `src` folder, the `program.py` package, as well as the `run()`-method inside `program.py`.
> These are fixed entry points for the service.
> Changing their names will result in a malfunctioning service.

## Run the project using Docker

You may utilize Docker to run your code locally and test your current implementation.
In general, by following the next steps you replicate the steps done by PLANQK, which is a way to verify your service in an early stage.

Build the Docker image:

```bash
docker build --pull -t python-starter .
```

Start the Docker container:

```bash
PROJECT_ROOT=(`pwd`)
docker run -it \
  -v $PROJECT_ROOT/input:/var/runtime/input \
  -v $PROJECT_ROOT/out:/var/runtime/output \
  -e PLANQK_PERSONAL_ACCESS_TOKEN=<your personal access token> \
  python-starter
```

> **HINT**
> For GitBash users on Windows, replace
> ```bash
> PROJECT_ROOT=(`pwd`)
> ```
> with
> ```bash
> PROJECT_ROOT=(/`pwd`)
> ```
>
> For Windows command-prompt users, you can use the following command:
> ```bash
> docker run -it \
>   -v %cd%/input:/var/runtime/input \
>   -v %cd%/out:/var/runtime/output \
>   -e PLANQK_PERSONAL_ACCESS_TOKEN=<your personal access token> \
>   python-starter
> ```

> **NOTE:**
> The filenames in the `input` directory must match the parameter names of the `run()` method in `program.py`.
> For example, a file `data.json` in the `input` directory will be passed as the `data` parameter to the `run()` method.

The container tries to serialize the return value of the `run()` method either to JSON or to a string.
It is then written to an `output.[json|txt]` file in the `output` directory.
When running the container like shown above, the file will be available on your host machine in the `out` directory.

## Next steps

Use `planqk up` to deploy your service to the PLANQK Platform.
Next, you may use `planqk run` to execute your service.
For more information, see the [PLANQK documentation](https://docs.planqk.de/quickstart.html).

However, you can also create a PLANQK Service manually via the PLANQK UI at <https://platform.planqk.de>.
To do so, you can use `planqk compress` to create a ZIP file that you can upload to the platform.

As soon as the service is ready to use, you will be able to run PLANQK Jobs to execute your service.
Further, you may publish it for internal use or into the PLANQK Marketplace to share it with other users.
