"""
Template for implementing services running on the PLANQK platform
"""
import os
import time
from typing import Dict, Any, Union, cast

from loguru import logger
from qiskit import QuantumCircuit, transpile
from qiskit_ibm_runtime import QiskitRuntimeService, Session, Sampler
from qiskit_ibm_runtime.accounts import ChannelType

from .libs.return_objects import ResultResponse, ErrorResponse


def run(data: Dict[str, Any] = None, params: Dict[str, Any] = None) -> Union[ResultResponse, ErrorResponse]:
    """
    Default entry point of your code. Start coding here!

    Parameters:
        data (Dict[str, Any]): The input data sent by the client
        params (Dict[str, Any]): Contains parameters, which can be set by the client to configure the execution

    Returns:
        response: (ResultResponse | ErrorResponse): Response as arbitrary json-serializable dict or an error to be passed back to the client
    """
    # defines the range of random numbers between 0 and 2^n_bits - 1
    n_bits = data.get("n_bits", 2)

    # initialize qiskit runtime service
    channel: ChannelType = cast(ChannelType, os.getenv("QISKIT_IBM_CHANNEL", "ibm_quantum"))
    token: str = os.getenv("QISKIT_IBM_TOKEN", None)
    instance: str = os.getenv("QISKIT_IBM_INSTANCE", "ibm-q/open/main")
    service = QiskitRuntimeService(channel=channel, token=token, instance=instance)

    backend = service.least_busy(simulator=False, operational=True)
    logger.info(f"Using backend: {backend}")

    # create circuit
    circuit = QuantumCircuit(n_bits, n_bits)
    circuit.h(range(n_bits))

    # perform measurement
    circuit.measure(range(n_bits), range(n_bits))

    # transpile circuit
    circuit = transpile(circuit, backend)

    start_time = time.time()
    logger.info("Acquiring session...")
    with Session(service, backend=backend, max_time=None) as session:
        sampler = Sampler(session=session)

        logger.info("Starting execution...")
        job = sampler.run(circuit, shots=10)
        logger.info(f"Job {job.job_id()} submitted")

        job_result = job.result()

        logger.info("Finished execution")
        execution_time = time.time() - start_time

        session.close()

    # extract random number
    random_number = int(list(job_result.quasi_dists[0].keys())[0])

    result = {
        "random_number": random_number,
    }
    metadata = {
        "execution_time": round(execution_time, 3),
    }

    logger.info("Calculation successfully executed")

    return ResultResponse(result=result, metadata=metadata)
