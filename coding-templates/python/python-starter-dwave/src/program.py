"""
Template for implementing services running on the PLANQK platform
"""
import time
from typing import Dict, Any, Union

import dimod
import numpy as np
from dwave.samplers import SimulatedAnnealingSampler
from dwave.system import LeapHybridSampler
from loguru import logger

from .libs.return_objects import ResultResponse, ErrorResponse


def run(data: Dict[str, Any] = None, params: Dict[str, Any] = None) -> Union[ResultResponse, ErrorResponse]:
    """
    Default entry point of your code. Start coding here!

    Parameters:
        data (Dict[str, Any]): The input data sent by the client
        params (Dict[str, Any]): Contains parameters, which can be set by the client to configure the execution

    Returns:
        response: (ResultResponse | ErrorResponse): Response as arbitrary json-serializable dict or an error to be passed back to the client
    """
    logger.info("D-Wave program started")
    start_time = time.time()

    # Defines whether to use a simulator or a real quantum device
    use_simulator = params.get('use_simulator', True)

    # You can run this code as is on the PLANQK Platform by setting a D-Wave Leap provider access token in your user
    # or organization settings. In addition, make sure you activate the option 'Add secrets to runtime environment' in
    # the service' runtime configuration,

    # If you run this code locally, make sure you set the 'DWAVE_API_TOKEN' environment variable, e.g., run it as follows:
    # > DWAVE_API_TOKEN=PQK-f0d9e... python -m src

    # Create a sampler and set any additional parameters you require
    if use_simulator:
        sampler = SimulatedAnnealingSampler()
    else:
        sampler = LeapHybridSampler(solver={"category": "hybrid"})
    logger.debug(f"Using sampler: {sampler}")

    # Create a random BQM and sample it
    bqm = dimod.generators.ran_r(1, 5)
    sample_set = sampler.sample(bqm)

    # Filter for solution with the lowest energy
    sample = sample_set.lowest()
    sample_result = next(sample.data(fields={"sample", "energy"}))

    eval_time = time.time() - start_time

    result = {
        "solution": {str(key): int(val) for key, val in sample_result.sample.items()}
    }
    metadata = {
        "energy": sample_result.energy,
        "eval_time": np.round(eval_time, 2),
    }

    logger.info("D-Wave program successfully executed")

    return ResultResponse(metadata=metadata, result=result)
