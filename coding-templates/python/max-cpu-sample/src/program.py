import math
import os
import threading
import time
from typing import Dict, Any

from loguru import logger


def find_prime_numbers(duration: int):
    start_time = time.time()

    count = 0
    num = 2

    while time.time() - start_time < duration:
        is_prime = True
        sqrt_num = math.isqrt(num)
        for i in range(2, sqrt_num + 1):
            if num % i == 0:
                is_prime = False
                break
        if is_prime:
            count += 1
        num += 1

    logger.info(f"Found <{count}> prime numbers in <{duration}> seconds")


def worker(thread_id: int):
    logger.info(f"Thread <{thread_id}> is started")

    find_prime_numbers(10)

    logger.info(f"Thread <{thread_id}> has finished")


def print_kwargs(**kwargs):
    if kwargs:
        for key, value in kwargs.items():
            logger.debug(f"{key}: {value}")


def run(**kwargs) -> Dict[str, Any]:
    print_kwargs(**kwargs)

    start_time = time.perf_counter()
    num_cpus = os.getenv("CPU_LIMIT", None) or os.cpu_count() or 1
    logger.info(f"Starting <{num_cpus}> threads...")

    threads = []

    for thread_id in range(int(num_cpus)):
        thread = threading.Thread(target=worker, args=(thread_id,))
        threads.append(thread)

        logger.info(f"Thread <{thread_id}> is starting...")
        thread.start()

    for thread in threads:
        thread.join()

    end_time = time.perf_counter()
    elapsed_time = end_time - start_time

    logger.info(f"All threads have finished in {elapsed_time} seconds")

    return {"num_cpus": num_cpus, "elapsed_time": elapsed_time}
