from planqk.commons.runtime import init_logging, ResponseHandler

from .program import run

init_logging()

result = run()

response = ResponseHandler(result)
response.print_json()
