# max-cpu-sample

## Development

```bash
conda env create -f environment.yml
conda activate max-cpu-sample
python3 -m src
```

## Run the project using Docker

You may utilize Docker to run your code locally and test your current implementation.
In general, by following the next steps you replicate the steps done by the PLANQK platform, which is a way to verify your service in an early stage.

### Build the Docker image

```bash
docker build --pull -t max-cpu-sample .

# or (for Apple M1 chips)
docker buildx build --pull -o type=docker --platform "linux/amd64" --tag max-cpu-sample .
```

### Start the Docker container

```bash
docker run -it max-cpu-sample
```
