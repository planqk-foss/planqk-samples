from planqk.commons.runtime import FileReader, ResponseHandler

with FileReader(["/var/input/data.json", "./input/data.json"]) as f:
    input_data = f.read_to_dict()

with FileReader(["/var/input/params.json", "./input/params.json"]) as f:
    input_params = f.read_to_dict()

if not input_data:
    print('Error: data.json file not found')
    exit(1)

# sum the values in the "values" property
sum_ = sum(input_data.get('values', []))

# round up the sum if "round_up" is true
result = {'sum': round(sum_ if not input_params.get('round_up') else sum_, 0)}

# handle the result
response = ResponseHandler(result)
response.print_json()
